﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace AlbumViewer.Business
{
    public static class DbInitializer
    {
        public static void Initialize(AlbumViewerContext context)
        {
            context.Database.EnsureCreated();

            Random rand = new Random();

            if (!context.Artists.Any())
            {
                try
                {
                    List<Model.Artist> artists = new List<Model.Artist>();
                    for (int i = 1; i < rand.Next(50,100); i++)
                    {
                        artists.Add(new Model.Artist()
                        {
                            AmazonUrl = $"http://amazon.com/artists/{i * rand.Next(5, 500)}",
                            ArtistName = $"ArtistNo{i}",
                            Description = $"DescriptionArtistNo{i}",
                            ImageUrl = $"http://www.scottchristensen.com.au/Images/artist-profile-picture.jpg"

                        });
                    }

                    int artistCount = 0;

                    foreach (var artist in artists)
                    {
                        artistCount++;
                        int albumCountToProduce = rand.Next(5, 20);

                        for (int i = 1; i <= albumCountToProduce; i++)
                        {
                            Model.Album album = new Model.Album()
                            {
                                Artist = artist,
                                Description = $"Album description nr {i} for {artist.ArtistName}",
                                Title = $"Album_{i}_Artist_{artistCount}",
                                Year = DateTime.Now.Year,
                                AmazonUrl = $"http://amazon.com/album/{i}",
                                SpotifyUrl = $"http://spotify.com",
                                ImageUrl = "https://cdn.pixabay.com/photo/2017/04/23/16/00/phonograph-2254165_960_720.jpg"
                            };


                            for (int j  = 1; j <= 18; j++)
                            {
                                Model.Track track = new Model.Track()
                                {
                                    SongName = $"Song {j} on album {i} for artist {artistCount}",
                                    UnitPrice = (j) * albumCountToProduce,
                                    Bytes = (j) * albumCountToProduce,
                                    Length = ((double)(rand.Next(150, 400) / 60)).ToString()
                                };

                                album.Tracks.Add(track);
                            }

                            context.Albums.Add(album);
                        }

                        
                    }

                    
                }
                catch (Exception ex)
                {

                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }

            }
            

            context.SaveChanges();
        }
    }
}
