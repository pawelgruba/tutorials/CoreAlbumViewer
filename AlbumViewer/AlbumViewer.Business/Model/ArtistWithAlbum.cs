﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlbumViewer.Business.Model
{
	public class ArtistWithAlbum : Artist
	{
		public int AlbumCount { get; set; }
	}
}
