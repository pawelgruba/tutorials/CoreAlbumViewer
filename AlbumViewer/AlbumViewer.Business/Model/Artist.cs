﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlbumViewer.Business.Model
{
	public class Artist
	{
		public int Id { get; set; }
		public string ArtistName { get; set; }
		public string Description { get; set; }
		public string ImageUrl { get; set; }
		public string AmazonUrl { get; set; }
	}
}
