﻿using AlbumViewer.Business.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace AlbumViewer.Business
{
	public class AlbumViewerContext : DbContext
	{
		public AlbumViewerContext(DbContextOptions options)
		: base(options)
        {
             
        }
		public DbSet<Album> Albums { get; set; }
		public DbSet<Artist> Artists { get; set; }
		public DbSet<Track> Tracks { get; set; }
		public DbSet<User> Users { get; set; }

		protected override void	OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);
		}
	}
	
}
