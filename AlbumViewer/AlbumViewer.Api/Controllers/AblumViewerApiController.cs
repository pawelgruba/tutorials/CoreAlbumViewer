using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AlbumViewer.Business;
using AlbumViewer.Business.Model;
using Microsoft.EntityFrameworkCore;

namespace AlbumViewer.Api.Controllers
{
	[Route("api")]
    public class AblumViewerApiController : Controller
    {
		private AlbumViewerContext Context;
		public AblumViewerApiController(AlbumViewerContext context)
		{
			Context = context;
		}

		[HttpGet("helloworld/{name}")]
		public object Get(string name)
		{
			return new 
			{
				message = $"Hello world, {name}",
				time = DateTime.Now
			};
		}

		[HttpGet("artists")]
		public async Task<List<Artist>> GetArtists()
		{
			return await Context.Artists
			.OrderBy(a => a.ArtistName)
			.ToListAsync();
		}
	}
}